# ======================================================================================================================
# Global
# ======================================================================================================================
stages:
  - build
  - static analysis
  - test
  - report
  - release
  - deploy

default:
  image: registry.gitlab.com/dependabot-gitlab/ci-images:ruby
  interruptible: true

variables:
  # App tags
  CURRENT_TAG: ${CI_COMMIT_REF_SLUG}-${CI_COMMIT_SHORT_SHA}
  # Docker images
  APP_IMAGE: ${CI_REGISTRY_IMAGE}/dev:${CURRENT_TAG}
  MOCK_IMAGE: thiht/smocker:0.18.0
  # Misc
  CODACY_VERSION: "13.5.3"
  DEPENDENCY_SCANNING_DISABLED: "true"
  SAST_DISABLED: "true"
  CONTAINER_SCANNING_DISABLED: "true"
  CODE_QUALITY_DISABLED: "true"

include:
  - local: .gitlab/ci/jobs.gitlab-ci.yml
  - local: .gitlab/ci/rules.gitlab-ci.yml

# ======================================================================================================================
# Pre Stage
# ======================================================================================================================
cache-dependencies:
  extends:
    - .rules:main
    - .cache_dependencies

# ======================================================================================================================
# Build Stage
# ======================================================================================================================
build-app-image:
  extends:
    - .build_app_image
    - .rules:build-image

# ======================================================================================================================
# Static analysis stage
# ======================================================================================================================
rubocop:
  extends:
    - .rubocop
    - .rules:main
  needs:
    - cache-dependencies

reek:
  extends:
    - .reek
    - .rules:main
  needs:
    - cache-dependencies

brakeman:
  extends:
    - .brakeman
    - .rules:main
  needs:
    - cache-dependencies

dependency-scan:
  extends:
    - .dependency_scan
    - .rules:dependency-scan
  needs:
    - cache-dependencies

# ======================================================================================================================
# Test Stage
# ======================================================================================================================
unit-test:
  extends:
    - .unit-test
    - .rules:main
  needs:
    - cache-dependencies

system-test:
  extends:
    - .system-test
    - .rules:main
  needs:
    - cache-dependencies

standalone-test:
  extends:
    - .standalone-test
    - .rules:main
  needs:
    - build-app-image

deploy-test:
  extends:
    - .deploy-test
    - .rules:main
  needs:
    - build-app-image

# ======================================================================================================================
# Reporting
# ======================================================================================================================
publish-allure-reports:
  extends:
    - .allure_report
    - .rules:allure-reports
  needs:
    - unit-test
    - system-test

publish-coverage:
  extends:
    - .coverage
    - .rules:coverage
  needs:
    - unit-test
    - system-test
    - cache-dependencies

# ======================================================================================================================
# Release Stage
# ======================================================================================================================
release-image:
  extends:
    - .release_image
    - .rules:release
  dependencies: []

create-gitlab-release:
  extends:
    - .gitlab_release
    - .rules:release
  dependencies: []

update-helm-chart:
  extends:
    - .update_chart
    - .rules:release
  needs: [release-image]

update-standalone-repo:
  extends:
    - .update_standalone
    - .rules:release
  needs: [release-image]

# ======================================================================================================================
# Deploy Stage
# ======================================================================================================================
deploy:
  extends:
    - .deploy
    - .rules:deploy
