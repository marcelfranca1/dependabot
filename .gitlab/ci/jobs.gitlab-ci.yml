include:
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - project: dependabot-gitlab/ci-images
    file: .gitlab/ci/runners.gitlab-ci.yml

# ======================================================================================================================
# Runners
# ======================================================================================================================
.gem_cache: &gem_cache
  key:
    files:
      - Gemfile.lock
  paths:
    - vendor/bundle
  policy: pull

.coverage_cache: &coverage_cache
  key: coverage-$CODACY_VERSION
  paths:
    - codacy-coverage-reporter-$CODACY_VERSION
  policy: pull

.ruby_runner:
  variables:
    BUNDLE_SUPPRESS_INSTALL_USING_MESSAGES: "true"
  before_script:
    - unset BUNDLE_APP_CONFIG
    - bundle config set --local path vendor/bundle
    - bundle install
  cache:
    - *gem_cache

.docker_runner:
  image: registry.gitlab.com/dependabot-gitlab/ci-images:docker-20.10
  services:
    - name: docker:20.10.17-dind
      alias: docker
  variables:
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: /certs
    DOCKER_CERT_PATH: /certs/client
    DOCKER_TLS_VERIFY: 1
    DOCKER_BUILDKIT: 1

.rspec_runner:
  stage: test
  extends: .ruby_runner
  services:
    - name: bitnami/redis:6.2-debian-10
      alias: redis
    - name: bitnami/mongodb:5.0-debian-10
      alias: mongodb
  variables:
    REDIS_URL: redis://redis:6379
    REDIS_PASSWORD: $REDIS_PASSWORD
    MONGODB_URL: mongodb:27017
    MAX_ROWS: 5
    OUTPUT_STYLE: block
    COVERAGE: "true"
    COV_DIR: reports/coverage/${CI_JOB_NAME}
  script:
    - |
      bundle exec rspec \
        --tag ${RSPEC_TAGS} \
        --format documentation \
        --format RspecJunitFormatter --out tmp/rspec.xml
  cache:
    - *gem_cache
  artifacts:
    reports:
      junit: tmp/rspec.xml
    paths:
      - reports/allure-results
      - reports/coverage
      - log/*.log
    expire_in: 1 day
    when: always

# ======================================================================================================================
# Jobs
# ======================================================================================================================

# ----------------------------------------------------------------------------------------------------------------------
# build stage
#
.cache_dependencies:
  stage: build
  extends: .ruby_runner
  variables:
    BUNDLE_FROZEN: "true"
  script:
    - .gitlab/script/download-coverage.sh
  cache:
    - <<: *gem_cache
      policy: pull-push
    - <<: *coverage_cache
      policy: pull-push

.build_app_image:
  stage: build
  extends: .buildkit_runner
  script: .gitlab/script/build-image.sh

# ----------------------------------------------------------------------------------------------------------------------
# 'static analysis' stage
#
.rubocop:
  stage: static analysis
  extends: .ruby_runner
  script:
    - bundle exec rubocop --parallel --color

.reek:
  stage: static analysis
  extends: .ruby_runner
  script:
    - bundle exec reek --color --progress --force-exclusion --sort-by smelliness .

.brakeman:
  stage: static analysis
  extends: brakeman-sast

.dependency_scan:
  stage: static analysis
  extends: gemnasium-dependency_scanning

# ----------------------------------------------------------------------------------------------------------------------
# test stage
#
.unit-test:
  extends: .rspec_runner
  variables:
    RSPEC_TAGS: ~system

.system-test:
  extends: .rspec_runner
  services:
    - name: ${MOCK_IMAGE}
      alias: smocker
    - name: bitnami/redis:6.2-debian-10
      alias: redis
    - name: bitnami/mongodb:5.0-debian-10
      alias: mongodb
  variables:
    MOCK_HOST: smocker
    GITLAB_URL: http://${MOCK_HOST}:8080
    RSPEC_TAGS: system
  before_script:
    - !reference [.ruby_runner, before_script]
    - .gitlab/script/build-bundler-helpers.sh

.standalone-test:
  stage: test
  extends: .docker_runner
  variables:
    COMPOSE_PROJECT_NAME: dependabot
  script:
    - .gitlab/script/run-standalone.sh
  after_script:
    - curl -X POST -s "http://docker:8081/sessions/verify" | jq

.deploy-test:
  stage: test
  extends: .docker_runner
  variables:
    SETTINGS__GITLAB_URL: http://gitlab:8080
    SETTINGS__DEPENDABOT_URL: http://ci-test.com
    SETTINGS__GITLAB_ACCESS_TOKEN: e2e-test
    SETTINGS__GITHUB_ACCESS_TOKEN: e2e-test
    SETTINGS__PROJECT_REGISTRATION: automatic
    SETTINGS__LOG_LEVEL: debug
    COMPOSE_PROJECT_NAME: dependabot
  before_script:
    - .gitlab/script/run-deploy.sh
  script:
    - .gitlab/script/test-deploy.sh
  after_script:
    - docker compose --ansi always logs migration web worker

# ----------------------------------------------------------------------------------------------------------------------
# test report stage
#
.allure_report:
  stage: report
  image:
    name: andrcuns/allure-report-publisher:0.8.0
    entrypoint: [""]
  variables:
    GITLAB_AUTH_TOKEN: $ALLURE_ACCESS_TOKEN
    ALLURE_JOB_NAME: rspec
    GOOGLE_CLOUD_KEYFILE_JSON: $KEYFILE_TEST_REPORTS
  script:
    - |
      allure-report-publisher upload gcs \
        --results-glob="reports/allure-results/*" \
        --bucket="allure-test-reports" \
        --prefix="dependabot-gitlab/$CI_COMMIT_REF_SLUG" \
        --update-pr="description" \
        --summary="behaviors" \
        --copy-latest \
        --color

.coverage:
  stage: report
  extends: .ruby_runner
  variables:
    MAX_ROWS: 5
    OUTPUT_STYLE: block
    NO_COLOR: 1
  coverage: /^COVERAGE:\s+(\d{1,3}\.\d{1,2})\%/
  script:
    - bundle exec rake coverage:merge
  after_script:
    - ./codacy-coverage-reporter-$CODACY_VERSION report -r coverage/coverage.xml
  cache:
    - *coverage_cache
    - *gem_cache
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/coverage.xml

# ----------------------------------------------------------------------------------------------------------------------
# release stage
#
.release_image:
  stage: release
  extends: .docker_runner
  variables:
    DOCKERHUB: docker.io/andrcuns/dependabot-gitlab
    GITLAB: $CI_REGISTRY_IMAGE
  before_script:
    - echo "$DOCKERHUB_PASSWORD" | docker login -u $DOCKERHUB_USERNAME --password-stdin
    - echo "$CI_REGISTRY_PASSWORD" | docker login $CI_REGISTRY -u $CI_REGISTRY_USER --password-stdin
  script:
    - .gitlab/script/release-image.sh
  interruptible: false

.gitlab_release:
  stage: release
  variables:
    RELEASE_NOTES_FILE: release_notes.md
  script:
    - .gitlab/script/changelog.sh
  release:
    tag_name: $CI_COMMIT_TAG
    description: $RELEASE_NOTES_FILE
  interruptible: false

.update_chart:
  extends: .ruby_runner
  stage: release
  script:
    - bundle exec rake "release:chart[$CI_COMMIT_TAG]"
  interruptible: false

.update_standalone:
  extends: .ruby_runner
  stage: release
  script:
    - bundle exec rake "release:standalone[$CI_COMMIT_TAG]"
  interruptible: false

# ----------------------------------------------------------------------------------------------------------------------
# deploy stage
#
.deploy:
  stage: deploy
  trigger:
    project: dependabot-gitlab/deploy
    strategy: depend
